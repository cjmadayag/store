<?php 
	require '../partials/template.php';

	function getTitle(){
		echo "Cart";
	}

	function getBodyContent(){
	?>
		<div class="container">
			<div class="col-lg-12">
				<table class="table table-bordered table-striped text-center">
					<thead>
						<tr>
							<td>ITEM</td>
							<td>PRICE</td>
							<td>QUANTITY</td>
							<td>SUBTOTAL</td>
							<td></td>
						</tr>	
					</thead>
					<tbody>
					<?php 
						require '../controllers/connection.php';
						session_start();

						foreach($_SESSION["cart"] as $id => $value){
							$item = mysqli_fetch_assoc(mysqli_query($myConn,"select * from items where id=$id"));
							$subtotal=$item["price"]*$value["quantity"];
						?>
							<tr>
								<td><?= $item["name"] ?></td>
								<td><?= $item["price"] ?></td>
								<td><?= $value["quantity"] ?></td>
								<td><?= $subtotal ?></td>
								<td>
									<a href="../controllers/remove-item-from-cart.php?id=<?= $id ?>" class="btn btn-danger btn-sm">Remove</a>
								</td>
							</tr>


						<?php
						}
					 ?>
					 </tbody>
				</table>
			</div>
		</div>


	<?php
	}
 ?>