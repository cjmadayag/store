<?php 
	require "../partials/template.php";

	function getTitle(){
		echo "Catalog";
	}

	function getBodyContent(){
	?>
	<div class="container py-3">
		<div class="row">
			<?php 
			require "../controllers/connection.php";

			$items = mysqli_query($myConn,"select * from items");

			foreach($items as $item){
			?>
				<div class="col-lg-3">
					<div class="card">

						<img src="<?= $item["imgPath"]; ?>" height=200px class="card-img-top">
						<div class="card-body">
							<p>Name: <?= $item["name"] ?></p>
							<p>Description: <?= $item["description"] ?></p>
							<p>Price: <?= $item["price"] ?></p>
						</div>
						<div class="card-footer text-center">
							<a href="edit-item.php?id=<?= $item["id"]; ?>" class="btn btn-warning">Edit</a>
							<a href="../controllers/delete-item-process.php?id=<?= $item["id"]; ?>" class="btn btn-danger">Delete</a>
						</div>
						<div class="card-footer text-center">
							<div class="d-flex justify-content-around align-items-center">
								<input type="number" name="quantity" class="form-control w-50 text-center" value="1">
								<button data-id="<?= $item["id"] ?>" class="btn btn-success p-2 addToCartBtn">Add to Cart</button>

							</div>
						</div>
					</div>
				</div>
			<?php
			}
			?>			
		</div>
	</div>
	<script type="text/javascript" src="../assets/scripts/add-to-cart.js" defer></script>
	<?php
	}
 ?>