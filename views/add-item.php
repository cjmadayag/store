<?php 
	require "../partials/template.php";

	function getTitle(){
		echo "Add Item";
	}

	function getBodyContent(){
	?>
		<div class="col-lg-4 offset-4">
			<form class="border rounded px-4 py-3" method="post" enctype="multipart/form-data" action="../controllers/add-item-process.php">
				<h1 class="text-center">Add Item</h1>
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" class="form-control">
				</div>	
				<div class="form-group">
					<label>Price</label>
					<input type="number" name="price" class="form-control">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea name="description" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label>Image</label>
					<input type="file" name="imgPath" class="form-control">
				</div>
				<div class="form-group">
					<label>Category</label>
					<select name="category_id" class="form-control">
						<?php 
							require '../controllers/connection.php';

							$categories = mysqli_query($myConn,"select * from categories");

							foreach($categories as $category){
							?>
								<option value="<?= $category["id"] ?>"><?= $category["name"] ?></option>
							<?php
							}
						?>
					</select>
				</div>
				<div class="text-center">
					<button class="btn btn-danger">Add Item</button>
				</div>
			</form>
		</div>
	<?php
	}
?>