<?php 
	require '../partials/template.php';

	function getTitle(){
		echo "Edit Item";
	}

	function getBodyContent(){
		require '../controllers/connection.php';
		$id = $_GET["id"];
		$item = mysqli_fetch_assoc(mysqli_query($myConn,"select * from items where id=$id"));
	?>
		<div class="col-lg-4 offset-4">
			<form class="border rounded px-4 py-3" method="post" enctype="multipart/form-data" action="../controllers/edit-item-process.php">
				<h1 class="text-center">Edit Item</h1>
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" class="form-control" value="<?= $item["name"]; ?>">
				</div>	
				<div class="form-group">
					<label>Price</label>
					<input type="number" name="price" class="form-control" value="<?= $item["price"]; ?>">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea name="description" class="form-control"><?= $item["description"]; ?></textarea>
				</div>
				<div class="form-group">
					<label>Image</label>
					<input type="file" name="imgPath" class="form-control">
				</div>
				<div class="form-group">
					<label>Category</label>
					<select name="category_id" class="form-control">
						<?php 
							require '../controllers/connection.php';

							$categories = mysqli_query($myConn,"select * from categories");

							foreach($categories as $category){
							?>
								<option value="<?= $category["id"] ?>"
									<?= $category["id"]==$item["category_id"] ? "selected" : ""; ?>><?= $category["name"] ?></option>
							<?php
							}
						?>
					</select>
				</div>
				<div class="text-center">
					<input type="hidden" name="id" value="<?= $item["id"]; ?>">
					<button class="btn btn-warning">Edit Item</button>
				</div>
			</form>
		</div>

	<?php
	}
?>

