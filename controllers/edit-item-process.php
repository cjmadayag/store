<?php
	require 'connection.php';
	$id = $_POST["id"];
	$name = $_POST["name"];
	$price = $_POST["price"];
	$description = $_POST["description"]; 
	$category_id = $_POST["category_id"];
	$imgPath_size = $_FILES["imgPath"]["size"];
	$imgPath_tmp = $_FILES["imgPath"]["tmp_name"];
	$imgPath_name = $_FILES["imgPath"]["name"];




	if(validation()){

		if($imgPath_size==0){
			$item = mysqli_fetch_assoc(mysqli_query($myConn,"select * from items where id=$id"));
			$imgPath = $item["imgPath"];
		}
		else{
			$imgPath = "../assets/images/".$imgPath_name;
			move_uploaded_file($imgPath_tmp, $imgPath);
		}

		$editItem = mysqli_query($myConn,"update items set name='$name',price=$price,description='$description',imgPath='$imgPath',category_id=$category_id where id=$id");

		header("location: ../views/catalog.php");
	}
	else{
		header("location:".$_SERVER["HTTP_REFERER"]);
	}

	function validation(){
		$extList = ["jpg","jpeg","png","gif","svg","webp","bitmap","tiff","tif"];

		$fileExt = strtolower(pathinfo($_FILES["imgPath"]["name"], PATHINFO_EXTENSION));

		$error=0;
		!isset($_POST["name"]) || $_POST["name"]=="" ? $error++:"";
		!isset($_POST["price"]) || $_POST["price"]=="" ? $error++:"";
		!isset($_POST["description"]) || $_POST["description"]=="" ? $error++:"";
		!isset($_POST["category_id"]) || $_POST["category_id"]=="" ? $error++:"";
		$_FILES["imgPath"]["size"]>0 && !in_array($fileExt, $extList) ? $error++:"";
		return $error>0? false:true;
	}
?>