<?php 
	session_start();

	$id = $_POST["id"];
	$quantity = $_POST["quantity"];

	isset($_SESSION["cart"][$id]["quantity"])? $_SESSION["cart"][$id]["quantity"] += $quantity : $_SESSION["cart"][$id]["quantity"] = $quantity;

	header("location:".$_SERVER["HTTP_REFERER"]);
?>