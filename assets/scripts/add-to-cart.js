let addToCartBtns = document.querySelectorAll(".addToCartBtn");


addToCartBtns.forEach(btnNode=>{
	btnNode.addEventListener("click",btn=>{
		let id = btn.target.dataset.id;
		let quantity = btn.target.previousElementSibling.value;

		if(quantity>0){
			let data = new FormData

			data.append("id",id);
			data.append("quantity",quantity);

			fetch("../controllers/add-to-cart-process.php",{
				method:"POST",
				body:data
			})


		}
		else{
			alert("Please enter valid quantity");
		}

	})
})

// fetch("../assets/scripts/test.php").then(res=>res.text()).then(res=>console.log(res))